#!/usr/bin/env php

<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Console\Application;

$application = new Application();
$commands = [];
foreach (glob(__DIR__ . '/../Console/Command/*.php') as $fileName)
{
    var_dump($fileName);

    $className = 'Samir\\M2Config\\Console\\Command\\' . basename($fileName, '.php');
    $commands[] = new $className();
}
$application->addCommands($commands);
$application->run();