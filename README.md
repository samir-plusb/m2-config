# M2 Config

This is a small and very simple command line script to create a config.local.php by a given yaml file. You can use it to
create config.local.php files for deployment.

## Installation

Since this module intends to be a Magento 2 module, you need add this module as a dependency in you composer.json file:

```
composer require samir/m2-config
```

## Usage

```
./bin/magento yaml_config /path/to/file.yaml [/path/to/config]
```

You can omit `path/to/config`. The default is `etc`. The script will create the config.local.php in this directory.

## Attention

Use at own risk! Some caveats:

- It will override any existing `config.local.php` files in the directory you specify
- only tested in own environment

