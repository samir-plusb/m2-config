<?php

namespace Samir\M2Config\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class GreetingCommand
 */
class YamlConfigCommand extends Command
{
    const ARGUMENT_SOURCE = 'source';

    const ARGUMENT_TARGET = 'target';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('yaml_config')
            ->setDescription('Create config.local.php by a yaml file')
            ->setDefinition([
                new InputArgument(
                    self::ARGUMENT_SOURCE,
                    InputArgument::REQUIRED,
                    'Source YAML file'
                ),
                new InputArgument(
                    self::ARGUMENT_TARGET,
                    InputArgument::OPTIONAL,
                    'Target destination folder for the config.local.php file. Defaults to "etc"',
                    'app/etc'
                ),

            ]);

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $source = $input->getArgument(self::ARGUMENT_SOURCE);
        $target = $input->getArgument(self::ARGUMENT_TARGET);

        if(!is_file($source)){
            throw new InvalidArgumentException(sprintf('File not found: %s', $source));
        }

        if(!is_dir($target)){
            mkdir($target, 0777, true);
        }

        $yamlConfig = Yaml::parse(file_get_contents($source));
        $targetFilepath = $target . DIRECTORY_SEPARATOR . 'config.local.php';
        $configFileContent = sprintf("<?php\n return %s;", var_export($yamlConfig, true));

        $result = file_put_contents($targetFilepath, $configFileContent);

        if($result){
            $output->writeln(sprintf('<info>Config file created: %s</info>', realpath($targetFilepath)));
        }

    }
}